﻿using UnityEngine;

public class PlayerDetector : MonoBehaviour
{
    public GameOverManagerNew gameOverManager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" && !other.isTrigger)
        {
            float enemyDistance = Vector3.Distance(transform.position,other.transform.position);
            gameOverManager.ShowWarning(enemyDistance);
        }
    }
}