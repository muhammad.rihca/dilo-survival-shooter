﻿
using UnityEngine;
using System;

public class EnemyFactory : MonoBehaviour
{

    [SerializeField]
    public GameObject[] enemyPrefab;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject FactoryMethod(int tag)
    {
        GameObject enemy = Instantiate(enemyPrefab[tag]);
        return enemy;
    }
}
