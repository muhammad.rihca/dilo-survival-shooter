﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootCommand : Command
{

    PlayerShootingNew playerShooting;

    public ShootCommand(PlayerShootingNew _playerShooting)
    {
        playerShooting = _playerShooting;
    }

    public override void Execute()
    {
        //Player menembak
        playerShooting.Shoot();
    }

    public override void UnExecute()
    {
        
    }
}