﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementNew : MonoBehaviour
{
    Transform player;
    PlayerHealthNew playerHealth;
    EnemyHealthNew enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;

    void Awake()
    {
        //Cari game object dengan tag player
        player = GameObject.FindGameObjectWithTag("Player").transform;

        //Mendapatkan Reference component
        playerHealth = player.GetComponent<PlayerHealthNew>();
        enemyHealth = GetComponent<EnemyHealthNew>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Memindahkan posisi player
        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            nav.SetDestination(player.position);
        }
        else //Hentikan moving
        {
            nav.enabled = false;
        }
    }
}
