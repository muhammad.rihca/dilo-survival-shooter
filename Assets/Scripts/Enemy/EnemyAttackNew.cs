﻿using UnityEngine;
using System.Collections;

public class EnemyAttackNew : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;


    Animator anim;
    GameObject player;
    PlayerHealthNew playerHealth;
    EnemyHealthNew enemyHealth;
    bool playerInRange = false;
    float timer;


    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealthNew>();
        enemyHealth = GetComponent<EnemyHealthNew>();
        anim = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        // print("triger");
        // if(other.gameObject == player && other.isTrigger == false)
        // {
        //     playerInRange = true;
        //     print("triger if");
        // }
        // Set player in range
        if (other.gameObject == player)
        {
            playerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        // if (other.gameObject == player)
        // {
            playerInRange = false;
        // }
    }


    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= timeBetweenAttacks && playerInRange /*&& enemyHealth.currentHealth > 0 */)
        {
            // print("attack en");
            Attack();

        }

        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("PlayerDead");
        }
    }


    void Attack()
    {
        timer = 0f;

        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }
}
